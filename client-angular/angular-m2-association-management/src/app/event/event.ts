export class Evenement{
    constructor(
        public title:string,
        public description:string,
        public eventDate:Date,
        public location:string,
        public price:number,
        public totalNumberOfPlaces:number,
        public remainingNumberOfPlaces:number
    ){}
}