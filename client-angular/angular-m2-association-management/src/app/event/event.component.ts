import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Evenement } from './event';
import { EventService } from './event.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {

  eventList: Evenement[] | undefined;
  event!: Evenement;
  constructor(private eventService:EventService, private router: Router) { }

  ngOnInit(){
    this.getAllEvent();
    this.event = new Evenement("","",new Date,"",0,0,0);
  }

  public getAllEvent(){
    return this.eventService.getAllEvent().subscribe(result => this.eventList = result);
  }
  public createEvent(){
    return this.eventService.createEvent(this.event).subscribe(data => {
      console.log(data);
      this.redirectToArticle();
    }
    );
  }

  redirectToArticle() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

}
