import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Evenement } from './event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }

  public getAllEvent(){
    return this.http.get<Evenement[]>("http://localhost:8080/association/events");
  }

  public createEvent(evenement: Evenement){
    return this.http.post<Evenement>("http://localhost:8080/association/events",evenement);
  }
}
