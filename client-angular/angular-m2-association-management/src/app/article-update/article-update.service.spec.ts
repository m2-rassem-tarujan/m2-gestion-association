import { TestBed } from '@angular/core/testing';

import { ArticleUpdateService } from './article-update.service';

describe('ArticleUpdateService', () => {
  let service: ArticleUpdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArticleUpdateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
