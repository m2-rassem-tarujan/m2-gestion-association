import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { News } from '../articles/news';
import { ArticleUpdateService } from './article-update.service';

@Component({
  selector: 'app-article-update',
  templateUrl: './article-update.component.html',
  styleUrls: ['./article-update.component.css']
})
export class ArticleUpdateComponent implements OnInit {

  id!: string;
  news!: News;

  constructor(private route: ActivatedRoute,private articleUpdateService:ArticleUpdateService, private router: Router) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.news = new News("","", "", "");
    this.articleUpdateService.getNewsById(this.id).subscribe( data => {
      this.news = data;
    });
  }
  
  public modifyNews(){
    return this.articleUpdateService.modifyNews(this.id, this.news).subscribe(data => {
      console.log(data);
      this.redirectToArticle();
    }
    );
  }

  redirectToArticle() {
    return this.router.navigate(['article']);
  }
  

}
