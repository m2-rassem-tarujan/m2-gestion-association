import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { News } from '../articles/news';

@Injectable({
  providedIn: 'root'
})
export class ArticleUpdateService {

  constructor(private http: HttpClient) { }

  public getAllNews(){
    return this.http.get<News[]>("http://localhost:8080/association/news");
  }

  public getNewsById(newsById:string){
    return this.http.get<News>("http://localhost:8080/association/news/" + newsById);
  }
  
  public modifyNews(id:string, newsModify:News) {
    return this.http.put<News>("http://localhost:8080/association/news/" + id, newsModify);
  }

}
