import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticleByIdComponent } from './article-by-id/article-by-id.component';
import { ArticleUpdateComponent } from './article-update/article-update.component';
import { ArticleUpdateService } from './article-update/article-update.service';
import { ArticlesComponent } from './articles/articles.component';
import { EventComponent } from './event/event.component';
import { AdherentComponent } from './adherents/adherent.component';
import { AccueilComponent } from './accueil/accueil.component';

const routes: Routes = [
  { path: 'article-update/:id', component:ArticleUpdateComponent},
  { path: 'article', component:ArticlesComponent},
  { path: 'article-by-id/:id', component:ArticleByIdComponent},
  { path: 'evenement', component:EventComponent},
  { path: '', component:AccueilComponent},
  { path: 'adherent', component:AdherentComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
export const routingComponents = [ArticleUpdateComponent]
