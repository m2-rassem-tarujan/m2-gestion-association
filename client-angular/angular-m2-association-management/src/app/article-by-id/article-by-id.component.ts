import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticlesService } from '../articles/articles.service';
import { News } from '../articles/news';

@Component({
  selector: 'app-article-by-id',
  templateUrl: './article-by-id.component.html',
  styleUrls: ['./article-by-id.component.css']
})
export class ArticleByIdComponent implements OnInit {
  id!:string;
  news!:News;

  constructor(private route: ActivatedRoute, private articleService:ArticlesService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.news = new News("","", "", "");
    this.articleService.getNewsById(this.id).subscribe( data => {
      this.news = data;
    });
  }

}
