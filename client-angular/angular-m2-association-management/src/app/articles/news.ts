export class News {
    constructor(
        public newsId:string,
        public title:string,
        public content:string,
        public author:string
    ){}
}