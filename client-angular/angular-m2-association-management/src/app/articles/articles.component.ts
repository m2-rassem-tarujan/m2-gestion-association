import { Component, OnInit } from '@angular/core';
import { ArticlesService } from './articles.service';
import { Router } from '@angular/router';
import { News } from './news';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  newsList: News[] | undefined;
  news!: News;

  constructor(private articleService:ArticlesService, private router: Router) { }

  ngOnInit(){
    this.getAllNews();
    this.news = new News("","","","");
  }

  public updateNews(id:string){
    this.router.navigate(['article-update', id]);
  }

  voirNewsById(id:string){
    this.router.navigate(['article-by-id', id]);
  }

  public getAllNews(){
    return this.articleService.getAllNews().subscribe(result => this.newsList = result);
  }
  public createNews(){
    return this.articleService.createNews(this.news).subscribe(data => {
      console.log(data);
      this.redirectToArticle();
    }
    );
  }
  public deleteNews(newsDelete:string){
    return this.articleService.deleteNews(newsDelete).subscribe(data => {
      console.log(data);
      this.redirectToArticle();
    }
    );
  }
  redirectToArticle() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

}
