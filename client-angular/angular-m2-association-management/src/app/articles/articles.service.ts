import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { News } from './news';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {

  constructor(private http: HttpClient) { }

  public getAllNews(){
    return this.http.get<News[]>("http://localhost:8080/association/news");
  }

  public createNews(news: News){
    return this.http.post<News>("http://localhost:8080/association/news",news);
  }
  public deleteNews(newsDelete:string) {
    return this.http.delete<News>("http://localhost:8080/association/news/" + newsDelete);
  }

  public getNewsById(newsById:string){
    return this.http.get<News>("http://localhost:8080/association/news/" + newsById);
  }
}
