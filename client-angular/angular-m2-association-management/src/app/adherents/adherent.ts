export class Adherent{
    constructor(
        public firstName:string,
        public lastName:string,
        public dateOfBirth:Date,
        public address:string,
        public email:string,
        public phoneNumber:string,
        public entryDate:Date,
        public isPremium:boolean
    ){}
}