import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Adherent } from './adherent';
import { AdherentService } from './adherent.service';

@Component({
  selector: 'app-adherent',
  templateUrl: './adherent.component.html',
  styleUrls: ['./adherent.component.css']
})

export class AdherentComponent implements OnInit {

  adherentList: Adherent[] | undefined;
  adherent!: Adherent;
  
  constructor(private adherentService:AdherentService, private router: Router) { }

  ngOnInit(){
    this.getAllAdherents();
    this.adherent = new Adherent("","",new Date,"","","",new Date,false);
  }

  public getAllAdherents(){
    return this.adherentService.getAllAdherents().subscribe(result => this.adherentList = result);
  }
  public createAdherent(){
    return this.adherentService.createAdherent(this.adherent).subscribe(data => {
      console.log(data);
      this.redirectToArticle();
    }
    );
  }
  
  redirectToArticle() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

}
