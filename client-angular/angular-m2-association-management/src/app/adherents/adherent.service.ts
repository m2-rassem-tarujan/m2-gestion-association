import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Adherent } from './adherent';

@Injectable({
  providedIn: 'root'
})
export class AdherentService {

  constructor(private http: HttpClient) { }

  public getAllAdherents(){
    return this.http.get<Adherent[]>("http://localhost:8080/association/adherents");
  }

  public createAdherent(adherent: Adherent){
    return this.http.post<Adherent>("http://localhost:8080/association/adherents",adherent);
  }

}
