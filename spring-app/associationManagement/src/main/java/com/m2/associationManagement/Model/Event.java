package com.m2.associationManagement.Model;

import java.util.Date;
import java.util.UUID;
import com.datastax.oss.driver.api.core.uuid.Uuids;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Event {

    @PrimaryKey
    private UUID eventId;

    private String title;
    private String description;
    private Date eventDate;
    private String location;
    private double price;
    private int totalNumberOfPlaces;
    private int remainingNumberOfPlaces;

    public Event() {}

    public Event(String title, String description, Date eventDate, String location, double price, int totalNumberOfPlaces) {
        this.eventId = Uuids.random();
        this.title = title;
        this.description = description;
        this.eventDate = eventDate;
        this.location = location;
        this.price = price;
        this.totalNumberOfPlaces = totalNumberOfPlaces;
        this.remainingNumberOfPlaces = totalNumberOfPlaces;
    }

    public UUID getId() {
        return eventId;
    }

    public void setId(UUID eventId) {
        this.eventId = eventId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getTotalNumberOfPlaces() {
        return totalNumberOfPlaces;
    }

    public void setTotalNumberOfPlaces(int totalNumberOfPlaces) {
        this.totalNumberOfPlaces = totalNumberOfPlaces;
    }

    public int getRemainingNumberOfPlaces() {
        return remainingNumberOfPlaces;
    }

    public void setRemainingNumberOfPlaces(int remainingNumberOfPlaces) {
        this.remainingNumberOfPlaces = remainingNumberOfPlaces;
    }

    public void takeOnePlace(){
        this.remainingNumberOfPlaces -- ;
    }

    @Override
    public String toString() {
        return "Event{" +
                "eventId=" + eventId +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", eventDate=" + eventDate +
                ", location='" + location + '\'' +
                ", price=" + price +
                ", totalNumberOfPlaces=" + totalNumberOfPlaces +
                ", remainingNumberOfPlaces=" + remainingNumberOfPlaces +
                '}';
    }

}
