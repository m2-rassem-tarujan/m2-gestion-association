package com.m2.associationManagement.Repository;

import java.util.List;
import java.util.UUID;

import com.m2.associationManagement.Model.Event;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface EventRepository extends CassandraRepository<Event, UUID> {
    List<Event> findByLocation(String location);
}