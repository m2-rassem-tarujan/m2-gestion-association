package com.m2.associationManagement.Controller;


import com.m2.associationManagement.Kafka.KafkaProducer;
import com.m2.associationManagement.Model.*;
import com.m2.associationManagement.Repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/association")
public class NewsPublicationController {

    @Autowired
    NewsRepository newsRepository;

    @Autowired
    NewsByCategoryRepository newsByCategoryRepository;

    @Autowired
    NewsByAuthorRepository newsByAuthorRepository;

    @Autowired
    KafkaProducer kafkaProducer;

    final String TOPIC = "association-news";

    @GetMapping(path = "/news")
    public ResponseEntity<List<News>> getAllNews() {
        try {
            List<News> newsList = new ArrayList<>();
            newsRepository.findAll().forEach(newsList::add);

            if (newsList.isEmpty()) {
                return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(newsList, HttpStatus.OK);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/news/{id}")
    public Optional<News> getNewsById(@PathVariable("id") UUID id) {
        return newsRepository.findById(id);
    }

    @PostMapping(path = "/news")
    public ResponseEntity<News> saveCar (@RequestBody News news) {
        try {
            News _news = newsRepository.save(new News(news.getTitle(), news.getContent(),news.getAuthor(), news.getCreatedAt()));
            kafkaProducer.sendMessage(TOPIC, _news.getNewsId().toString());
            return new ResponseEntity<>(_news, HttpStatus.CREATED);
        }
        catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.EXPECTATION_FAILED);
        }
    }
    /*
    @PostMapping("/news")
    public News createCategory(@RequestBody News news) {
        return newsRepository.save(new News(
                news.getTitle(), news.getContent(), news.getAuthor(), news.getCreatedAt()
        ));
    }*/

    @PutMapping("/news/{id}")
    public News updateEmployee(@PathVariable("id") UUID id, @RequestBody News news) {
        Optional<News> newsData = newsRepository.findById(id);

        if (newsData.isPresent()) {

            News instance = newsData.get();
            instance.setNewsId(news.getNewsId());
            instance.setTitle(news.getTitle());
            instance.setContent(news.getContent());
            instance.setAuthor(news.getAuthor());
            instance.setCreatedAt(news.getCreatedAt());
            newsRepository.deleteById(instance.getNewsId());

            return newsRepository.save(instance);

        } else {
            return null;
        }
    }

    @DeleteMapping("/news/{id}")
    public void deleteEmployee(@PathVariable("id") UUID id) {
        newsRepository.deleteById(id);
    }

    @GetMapping("/news/category/{category_id}/publications")
    public List<NewsByCategory> getPublicationsByCategory(@PathVariable("category_id") UUID category_id) {
        return newsByCategoryRepository.getNewsByAuthorBy(category_id);
    }

    @GetMapping("/news/author/{employee_id}/publications")
    public List<NewsByAuthor> getPublicationsByAuthor(@PathVariable("employee_id") UUID employee_id) {
        return newsByAuthorRepository.getNewsByAuthorBy(employee_id);
    }

}

