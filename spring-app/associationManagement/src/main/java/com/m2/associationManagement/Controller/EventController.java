package com.m2.associationManagement.Controller;

import com.m2.associationManagement.Kafka.KafkaProducer;
import com.m2.associationManagement.Model.Event;
import com.m2.associationManagement.Repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/association")
public class EventController {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    KafkaProducer kafkaProducer;

    final String TOPIC = "association-events";

    @GetMapping("/events")
    public List<Event> getAllEvents() {
        return eventRepository.findAll();
    }

    @GetMapping("/events/{id}")
    public Optional<Event> getEventById(@PathVariable("id") UUID id) {
        return eventRepository.findById(id);
    }

    @GetMapping("/events/locations/{location}")
    public List<Event> getEventByLocation(@PathVariable("location") String location) {
        return eventRepository.findByLocation(location);
    }

    @PostMapping("/events")
    public Event createEvent(@RequestBody Event e) {
        Event event = new Event(
                e.getTitle(), e.getDescription(),e.getEventDate(), e.getLocation(), e.getPrice(), e.getTotalNumberOfPlaces()
        );

        kafkaProducer.sendMessage(TOPIC, event.getId().toString());
        return eventRepository.save(event);
    }

    @PutMapping("/events/{id}")
    public Event updateEvent(@PathVariable("id") UUID id, @RequestBody Event e) {
        Optional<Event> eventData = eventRepository.findById(id);

        if (eventData.isPresent()) {

            Event instance = eventData.get();
            instance.setTitle(e.getTitle());
            instance.setDescription(e.getDescription());
            instance.setEventDate(e.getEventDate());
            instance.setLocation(e.getLocation());
            instance.setPrice(e.getPrice());
            instance.setTotalNumberOfPlaces(e.getTotalNumberOfPlaces());

            return eventRepository.save(instance);

        } else {
            return null;
        }
    }

    @DeleteMapping("/events/{id}")
    public void deleteEvent(@PathVariable("id") UUID id) {
        eventRepository.deleteById(id);
    }

}
