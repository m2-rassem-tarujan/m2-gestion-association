package com.m2.associationManagement.Repository;

import com.m2.associationManagement.Model.News;
import org.springframework.data.cassandra.repository.CassandraRepository;

import java.util.UUID;

public interface NewsRepository extends CassandraRepository<News, UUID> {

}

