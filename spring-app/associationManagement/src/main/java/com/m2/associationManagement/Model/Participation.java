package com.m2.associationManagement.Model;

import java.util.UUID;

public class Participation {

    private UUID adherentId ;
    private UUID eventId ;

    private String title ;
    private String eventLocation ;
    private double paid_price ;
    private String firstName ;
    private String lastName ;
    private String  email ;

    public Participation(){}

    public Participation(UUID eventId, String title, String eventLocation, double paid_price, UUID adherentId, String firstName, String lastName, String email) {
        this.eventId = eventId;
        this.title = title;
        this.eventLocation = eventLocation;
        this.paid_price = paid_price;
        this.adherentId = adherentId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public UUID getEventId() {
        return eventId;
    }

    public void setEventId(UUID eventId) {
        this.eventId = eventId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEventLocation() {
        return eventLocation;
    }

    public void setEventLocation(String eventLocation) {
        this.eventLocation = eventLocation;
    }

    public double getPaid_price() {
        return paid_price;
    }

    public void setPaid_price(double paid_price) {
        this.paid_price = paid_price;
    }

    public UUID getAdherentId() {
        return adherentId;
    }

    public void setAdherentId(UUID adherentId) {
        this.adherentId = adherentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
