package com.m2.associationManagement.Model;

import java.sql.Date;
import java.util.UUID;

public class Publication {

    private UUID newsId;
    private UUID categoryId;

    private String title;
    private String content;
    private String author;
    private Date createdAt;

    private String categoryName;
    private UUID employeeId;
    private String firstName;
    private String lastName;

    public Publication(){

    }

    public Publication(UUID newsId, UUID categoryId, String title, String content, String author, Date createdAt, String categoryName, UUID employeeId, String firstName, String lastName){
        this.newsId = newsId;
        this.categoryId = categoryId;
        this.title = title;
        this.content = content;
        this.author = author;
        this.createdAt = createdAt;
        this.categoryName = categoryName;
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.lastName = lastName;

    }


    public UUID getNewsId() {
        return newsId;
    }

    public void setNewsId(UUID newsId) {
        this.newsId = newsId;
    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public UUID getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(UUID employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
