package com.m2.associationManagement.Repository;

import com.m2.associationManagement.Model.NewsByAuthor;
import com.m2.associationManagement.Model.NewsByCategory;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.util.List;
import java.util.UUID;

public interface NewsByCategoryRepository extends CassandraRepository<NewsByCategory, UUID> {
    @Query("SELECT * FROM association.news_by_author WHERE categoryId=?0")
    List<NewsByCategory> getNewsByAuthorBy(UUID categoryId);
}
