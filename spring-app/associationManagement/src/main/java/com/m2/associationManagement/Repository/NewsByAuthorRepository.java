package com.m2.associationManagement.Repository;

import com.m2.associationManagement.Model.EventByAdherent;
import com.m2.associationManagement.Model.NewsByAuthor;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.util.List;
import java.util.UUID;

public interface NewsByAuthorRepository extends CassandraRepository<NewsByAuthor, UUID> {
    @Query("SELECT * FROM association.news_by_author WHERE employeeId=?0")
    List<NewsByAuthor> getNewsByAuthorBy(UUID employeeId);
}
