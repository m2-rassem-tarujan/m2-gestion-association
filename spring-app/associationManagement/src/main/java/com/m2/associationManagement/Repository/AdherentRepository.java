package com.m2.associationManagement.Repository;

import com.m2.associationManagement.Model.Adherent;
import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.util.List;
import java.util.UUID;

public interface AdherentRepository extends CassandraRepository<Adherent, UUID> {

    @Query("SELECT * FROM adherent WHERE isPremium=true")
    List<Adherent> findPremium();

}
