package com.m2.associationManagement.Model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.sql.Date;
import java.util.UUID;

@Table("news_by_category")
public class NewsByCategory extends Publication{
    @PrimaryKey
    public UUID newsByCategoryId;

    public NewsByCategory(UUID newsByCategoryId, UUID newsId, UUID categoryId, String title, String content, String author, Date createdAt, String categoryName, UUID employeeId, String firstName, String lastName){
        super(newsId, categoryId, title, content, author, createdAt, categoryName, employeeId, firstName, lastName);
        this.newsByCategoryId = newsByCategoryId;
    }

    public UUID getNewsByCategoryId() {
        return newsByCategoryId;
    }

    public void setNewsByCategoryId(UUID newsByCategoryId) {
        this.newsByCategoryId = newsByCategoryId;
    }
}
