package com.m2.associationManagement.Model;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.sql.Date;
import java.util.UUID;

@Table("news_by_author")
public class NewsByAuthor extends Publication{

    @PrimaryKey
    public UUID newsByAuthorId;

    public NewsByAuthor(UUID newsByAuthorId, UUID newsId, UUID categoryId, String title, String content, String author, Date createdAt, String categoryName, UUID employeeId, String firstName, String lastName){
        super(newsId, categoryId, title, content, author, createdAt, categoryName, employeeId, firstName, lastName);
        this.newsByAuthorId = newsByAuthorId;
    }
    public UUID getNewsByAuthorId() {
        return newsByAuthorId;
    }

    public void setNewsByAuthorId(UUID newsByAuthorId) {
        this.newsByAuthorId = newsByAuthorId;
    }
}
