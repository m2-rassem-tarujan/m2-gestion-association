package com.m2.associationManagement.Repository;

import com.m2.associationManagement.Model.AdherentByEvent;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AdherentByEventRepository extends CassandraRepository<AdherentByEvent, UUID> {

    @Query("SELECT * FROM association.adherent_by_event WHERE eventId=?0")
    List<AdherentByEvent> getParticipations(UUID eventId);

    @Query("SELECT * FROM association.adherent_by_event WHERE eventId=?0 AND adherentId=?1")
    Optional<AdherentByEvent> getOne(UUID eventId, UUID adherentId);


}
