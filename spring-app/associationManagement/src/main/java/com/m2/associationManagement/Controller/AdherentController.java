package com.m2.associationManagement.Controller;

import com.m2.associationManagement.Model.Adherent;
import com.m2.associationManagement.Repository.AdherentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/association")
public class AdherentController {

    @Autowired
    AdherentRepository adherentRepository;

    @GetMapping("/adherents")
    public List<Adherent> getAllAdherents() {
        return adherentRepository.findAll();
    }

    @GetMapping("/adherents/premium")
    public List<Adherent> getPremiumAdherents() {
        return adherentRepository.findPremium();
    }

    @GetMapping("/adherents/{id}")
    public Optional<Adherent> getAdherentById(@PathVariable("id") UUID id) {
        return adherentRepository.findById(id);
    }

    @PostMapping("/adherents")
    public Adherent createAdherent(@RequestBody Adherent e) {
        return adherentRepository.save(new Adherent(
                e.getFirstName(), e.getLastName(), e.getDateOfBirth(), e.getAddress(), e.getEmail(), e.getPhoneNumber(), e.getEntryDate(), e.isPremium()
        ));
    }

    @PutMapping("/adherents/{id}")
    public Adherent updateAdherent(@PathVariable("id") UUID id, @RequestBody Adherent e) {
        Optional<Adherent> AdherentData = adherentRepository.findById(id);

        if (AdherentData.isPresent()) {

            Adherent instance = AdherentData.get();
            instance.setFirstName(e.getFirstName());
            instance.setLastName(e.getLastName());
            instance.setDateOfBirth(e.getDateOfBirth());
            instance.setAddress(e.getAddress());
            instance.setEmail(e.getEmail());
            instance.setPhoneNumber(e.getPhoneNumber());
            instance.setEntryDate(e.getEntryDate());
            instance.setPremium(e.isPremium());

            return adherentRepository.save(instance);

        } else {
            return null;
        }
    }

    @DeleteMapping("/Adherents/{id}")
    public void deleteAdherent(@PathVariable("id") UUID id) {
        adherentRepository.deleteById(id);
    }
}
