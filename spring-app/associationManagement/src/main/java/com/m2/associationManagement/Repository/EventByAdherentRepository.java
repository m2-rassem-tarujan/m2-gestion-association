package com.m2.associationManagement.Repository;

import com.m2.associationManagement.Model.EventByAdherent;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.util.List;
import java.util.UUID;

public interface EventByAdherentRepository extends CassandraRepository<EventByAdherent, UUID> {

    @Query("SELECT * FROM association.event_by_adherent WHERE adherentId=?0")
    List<EventByAdherent> getParticipations(UUID adherentId);
}
