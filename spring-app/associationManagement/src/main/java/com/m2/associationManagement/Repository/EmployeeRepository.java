package com.m2.associationManagement.Repository;

import com.m2.associationManagement.Model.Employee;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.cassandra.repository.Query;

import java.util.List;
import java.util.UUID;

public interface EmployeeRepository extends CassandraRepository<Employee, UUID> {

    @Query("SELECT * FROM employee WHERE isAdmin=true")
    List<Employee> findAdmins();

}
