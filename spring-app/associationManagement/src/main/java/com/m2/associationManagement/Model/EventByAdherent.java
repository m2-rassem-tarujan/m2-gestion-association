package com.m2.associationManagement.Model;
import com.datastax.oss.driver.api.core.uuid.Uuids;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import java.util.UUID;

@Table("event_by_adherent")
public class EventByAdherent extends Participation {

    @PrimaryKey
    private UUID participationId;

    public EventByAdherent(UUID participationId,UUID eventId, String title, String eventLocation, double paid_price, UUID adherentId, String firstName, String lastName, String email){
        super(eventId,title,eventLocation,paid_price,adherentId,firstName, lastName,email);
        this.participationId = participationId;
    }

    public UUID getParticipationId() {
        return participationId;
    }

    public void setParticipationId(UUID participationId) {
        this.participationId = participationId;
    }
}
