package com.m2.associationManagement.Controller;

import com.m2.associationManagement.Model.Employee;
import com.m2.associationManagement.Repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/association")
public class EmployeeController {
    
    @Autowired
    EmployeeRepository employeeRepository;

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @GetMapping("/employees/admin")
    public List<Employee> getAdminEmployees() {
        return employeeRepository.findAdmins();
    }

    @GetMapping("/employees/{id}")
    public Optional<Employee> getEmployeeById(@PathVariable("id") UUID id) {
        return employeeRepository.findById(id);
    }

    @PostMapping("/employees")
    public Employee createEmployee(@RequestBody Employee e) {
        return employeeRepository.save(new Employee(
               e.getFirstName(), e.getLastName(), e.getDateOfBirth(), e.getAddress(), e.getEmail(), e.getPhoneNumber(), e.getJob(), e.getSalary(), e.isAdmin()
        ));
    }

    @PutMapping("/employees/{id}")
    public Employee updateEmployee(@PathVariable("id") UUID id, @RequestBody Employee e) {
        Optional<Employee> employeeData = employeeRepository.findById(id);

        if (employeeData.isPresent()) {

            Employee instance = employeeData.get();
            instance.setFirstName(e.getFirstName());
            instance.setLastName(e.getLastName());
            instance.setDateOfBirth(e.getDateOfBirth());
            instance.setAddress(e.getAddress());
            instance.setEmail(e.getEmail());
            instance.setPhoneNumber(e.getPhoneNumber());
            instance.setJob(e.getJob());
            instance.setSalary(e.getSalary());
            instance.setAdmin(e.isAdmin());

            return employeeRepository.save(instance);

        } else {
            return null;
        }
    }

    @DeleteMapping("/employees/{id}")
    public void deleteEmployee(@PathVariable("id") UUID id) {
        employeeRepository.deleteById(id);
    }
}
