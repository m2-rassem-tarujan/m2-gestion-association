package com.m2.associationManagement.Controller;

import com.m2.associationManagement.Model.Category;
import com.m2.associationManagement.Repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/association")
public class CategoryController {

    @Autowired
    CategoryRepository categoryRepository;

    @GetMapping("/category")
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @GetMapping("/category/{id}")
    public Optional<Category> getCategoryById(@PathVariable("id") UUID id) {
        return categoryRepository.findById(id);
    }

    @PostMapping("/category")
    public Category createCategory(@RequestBody Category category) {
        return categoryRepository.save(new Category(
                category.getCategoryName(), category.getDescription()
        ));
    }

    @PutMapping("/category/{id}")
    public Category updateEmployee(@PathVariable("id") UUID id, @RequestBody Category category) {
        Optional<Category> categoryData = categoryRepository.findById(id);

        if (categoryData.isPresent()) {

            Category instance = categoryData.get();
            instance.setCategoryId(category.getCategoryId());
            instance.setCategoryName(category.getCategoryName());
            instance.setDescription(category.getDescription());

            return categoryRepository.save(instance);

        } else {
            return null;
        }
    }

    @DeleteMapping("/category/{id}")
    public void deleteEmployee(@PathVariable("id") UUID id) {
        categoryRepository.deleteById(id);
    }
}
