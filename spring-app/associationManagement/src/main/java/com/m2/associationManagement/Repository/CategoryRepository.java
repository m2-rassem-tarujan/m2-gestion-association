package com.m2.associationManagement.Repository;

import com.m2.associationManagement.Model.Category;
import org.springframework.data.cassandra.repository.CassandraRepository;

import java.util.UUID;

public interface CategoryRepository extends CassandraRepository<Category, UUID> {
}
