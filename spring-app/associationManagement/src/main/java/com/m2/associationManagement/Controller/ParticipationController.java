package com.m2.associationManagement.Controller;

import com.datastax.oss.driver.api.core.uuid.Uuids;
import com.m2.associationManagement.Model.*;
import com.m2.associationManagement.Repository.AdherentByEventRepository;
import com.m2.associationManagement.Repository.AdherentRepository;
import com.m2.associationManagement.Repository.EventByAdherentRepository;
import com.m2.associationManagement.Repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/association")
public class ParticipationController {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    AdherentRepository adherentRepository;

    @Autowired
    EventByAdherentRepository eventByAdherentRepository;

    @Autowired
    AdherentByEventRepository adherentByEventRepository;

    @GetMapping("/events/participations")
    public ResponseEntity<String> addParticipation(@RequestParam String ide, @RequestParam String ida) {

        UUID event_id = UUID.fromString(ide);
        UUID adherent_id = UUID.fromString(ida);

        Optional<Event> eventData = eventRepository.findById(event_id);
        Optional<Adherent> adherentData = adherentRepository.findById(adherent_id);

        if (eventData.isPresent() && adherentData.isPresent()) {
            Event event = eventData.get();
            Adherent adherent = adherentData.get();

            if (event.getRemainingNumberOfPlaces() > 0){

                if(adherentByEventRepository.getOne(event_id,adherent_id).isEmpty()){
                    UUID participationId = Uuids.random();

                    eventByAdherentRepository.save(new EventByAdherent(
                            participationId,event.getId(),event.getTitle(),event.getLocation(),event.getPrice(),adherent.getAdherentId(),adherent.getFirstName(),adherent.getLastName(),adherent.getEmail()
                    ));

                    adherentByEventRepository.save(new AdherentByEvent(
                            participationId,event.getId(),event.getTitle(),event.getLocation(),event.getPrice(),adherent.getAdherentId(),adherent.getFirstName(),adherent.getLastName(),adherent.getEmail()
                    ));

                    event.takeOnePlace();
                    eventRepository.save(event);

                    return new ResponseEntity<String>("Successfully registered !", HttpStatus.CREATED);
                }
                else {
                    return new ResponseEntity<String>("Hi, your are already registered for this event !", HttpStatus.BAD_REQUEST);
                }
            }
            else {
                return new ResponseEntity<String>("Sorry, all the places are already taken for this event.", HttpStatus.OK);
            }
        }

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/events/{event_id}/participants")
    public List<AdherentByEvent> getParticipantsByEvent(@PathVariable("event_id") UUID event_id) {
        return adherentByEventRepository.getParticipations(event_id);
    }

    @GetMapping("/adherents/{adherent_id}/participations")
    public List<EventByAdherent> getParticipationsByAdherent(@PathVariable("adherent_id") UUID adherent_id) {
        return eventByAdherentRepository.getParticipations(adherent_id);
    }

}