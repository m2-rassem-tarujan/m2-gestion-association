package com.m2.associationManagement.Model;

import com.datastax.oss.driver.api.core.uuid.Uuids;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import java.util.UUID;

@Table
public class Category {

    @PrimaryKey
    private UUID categoryId;
    private String categoryName;
    private String description;


    public Category() {

    }

    public Category(String categoryName, String description){
        this.categoryId = Uuids.random();
        this.categoryName = categoryName;
        this.description = description;

    }

    public UUID getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(UUID categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Category{" +
                "categoryId=" + categoryId +
                ", categoryName=" + categoryName +
                ", description=" + description +
                '}';
    }


}

