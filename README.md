## Projet universitaire de programmation web distribuée

## Solution Full Stack pour la gestion d'une association en ligne

### Composants de l'architecture

    - Base de données NoSQL : Cassandra
    - API REST : Spring Boot
    - Client : Angular
    - Message broker : Apache Kafka
    - Programme d'envoi de mails : Python

### Installation : docker-compose up