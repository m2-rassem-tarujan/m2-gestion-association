from kafka import KafkaConsumer
from cassandra.cluster import Cluster
from cassandra.query import dict_factory
from mailer import Mailer

# Web server base url
base_url = 'http://localhost:8080/association'

# Mailer setup
host = 'smtp.gmail.com'
port = 587
sender = 'rassem.tarujan@gmail.com'

with open('key.txt', 'r') as file:
    password = file.read().replace('\n','')

mailer = Mailer(host,port,sender,password)

event_email_template = mailer.read_template('public/event_email_template.html')
news_email_template = mailer.read_template('public/news_email_template.html')

# Database connection
cluster = Cluster(['cassandra'], port=9042)
session = cluster.connect('association')
session.row_factory = dict_factory # result : each row is a dict

# Kafka consumer
consumer = KafkaConsumer(
    bootstrap_servers=['kafka:9092'],
    auto_offset_reset='earliest',
    enable_auto_commit=True,
    group_id='group_id',
    value_deserializer=lambda x: x.decode('utf-8')
)

# Topic handlers
def assoc_events_handler(event_id):
    req = f'SELECT * FROM association.event WHERE eventId={event_id}'
    rows = session.execute(req)
    # some times i get empty result !
    if(not rows): session.execute(req)

    if rows:
        event = rows.one()
        req = 'SELECT * FROM association.adherent'
        rows = session.execute(req)

        for adherent in rows:
            mailer.send(sender,adherent['email'],'Invitation Nouvel Evénement',event_email_template.substitute(
                title=event['title'],description=event['description'],event_date=event['eventdate'],location=event['location'],price=event['price'],
                link=f"{base_url}/events/participations?ide={event['eventid']}&ida={adherent['adherentid']}"
            ))

def assoc_news_handler(news_id):
    req = f'SELECT * FROM association.news WHERE newsId={news_id}'
    rows = session.execute(req)
    # some times i get empty result !
    if(not rows): session.execute(req)

    if rows:
        news = rows.one()
        req = 'SELECT * FROM association.adherent'
        rows = session.execute(req)

        for adherent in rows:
            mailer.send(sender,adherent['email'],'Association Newsletter',news_email_template.substitute(
                title=news['title'],content=news['content'],author=news['author']
            ))

# Process messages

TOPICS = {
    'association-events':assoc_events_handler,
    'association-news': assoc_news_handler
}

consumer.subscribe([* TOPICS])

for message in consumer:
    TOPICS[message.topic](message.value)

# END
session.close()
cluster.close()
mailer.quit()