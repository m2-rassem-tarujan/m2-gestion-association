import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from string import Template

class Mailer:

    def __init__(self,host,port,sender,password):
        # set up the SMTP server
        self.server = smtplib.SMTP(host,port)
        self.server.starttls()
        self.server.login(sender,password)

    def send(self,sender,receiver,subject,body):
        msg = MIMEMultipart()
        msg['From']= sender
        msg['To']=receiver
        msg['Subject']=subject
        msg.attach(MIMEText(body,'html'))
        email_content = msg.as_string()
        self.server.sendmail(sender,receiver,email_content)
        del msg

    def read_template(self,filename):
        with open(filename, 'r', encoding='utf-8') as template_file:
            template_file_content = template_file.read()
        return Template(template_file_content)

    def quit(self):
        self.server.quit()
