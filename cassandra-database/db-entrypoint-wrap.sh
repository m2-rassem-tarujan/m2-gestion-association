#!/bin/bash

if [[ ! -z "$CREATE_SCHEMA" && $1 = 'cassandra' ]]; then
  until cqlsh -f /db-schema.cql ; do
    echo "cqlsh: Cassandra is unavailable - retry later"
    sleep 5
  done &
fi

exec /docker-entrypoint.sh "$@"